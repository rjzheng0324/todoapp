import React from 'react';
import ReactDOM from 'react-dom';
import { Route, Router, IndexRoute, browserHistory } from 'react-router';
import TodoApp from 'TodoApp';

ReactDOM.render(
  <TodoApp />,
  document.getElementById('app')
);
